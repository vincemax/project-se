package bavards;

public class Starter {

	public static void main(String args[]) throws InterruptedException {
		BavardA bavardsA[];
		BavardB bavardsB[];
		
		if (args.length != 2) {
				System.out.println("Usage: Starter <nb bavards nb loops>");
				return;
		}

		int nbavards = new Integer(args[0]).intValue();
		int nloops = new Integer(args[1]).intValue();

		bavardsA = new BavardA[nbavards];
		bavardsB = new BavardB[nbavards];

		for (int i = 0; i < nbavards; i++){
			bavardsA[i] = new BavardA(i, nloops);
			bavardsB[i] = new BavardB(i+nbavards, nloops);
		}

		for (int i = 0; i < nbavards; i++){
			bavardsA[i].join();
			bavardsB[i].join();
		}
		System.out.println("End of program");
	}

	public static void print(String threadType, int threadId, int step) {
		System.out.println("thread:" + threadType + 
				" id:" + threadId + " step:" + step + "\n");
	}
}

