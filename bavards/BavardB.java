package bavards;


public class BavardB implements Runnable  { 
	int nloops;
	int id;
	Thread t;

	public BavardB(int id, int nloops) {
		this.id = id;
		this.nloops = nloops;
		t = new Thread(this);
		t.start();  
	}

	public void run() {
		for (int i = 0; i < nloops; i++) {
			Starter.print("BavardB", id, i);
			Thread.yield();
		}
		System.out.println("BavardB " + id + " completed\n");
	}

	public void join()throws InterruptedException {
		t.join();     
	}
}

