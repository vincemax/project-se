package bavards;

public  class BavardA extends Thread {
	int nloops;
	int id;

	public BavardA(int id, int nloops) {
		this.id = id;
		this.nloops = nloops;
		this.start();  
	}
	public void run() {
		for (int i = 0; i < nloops; i++) {
			Starter.print("BavardA", id, i);
			Thread.yield();
		}
		System.out.println("BavardA " + id + " completed\n");
	}
}
