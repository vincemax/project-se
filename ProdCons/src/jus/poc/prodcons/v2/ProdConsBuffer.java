package jus.poc.prodcons.v2;

import java.util.concurrent.Semaphore;

import jus.poc.prodcons.IProdConsBuffer;
import jus.poc.prodcons.Message;

public class ProdConsBuffer implements IProdConsBuffer {
	Semaphore mutex, notFull, notEmpty;
	Message buffer[];
	int taillebuffer;
	int tete;
	int queue;
	public int nbMaxMsg;

	public ProdConsBuffer(int taillebuffer) {
		this.taillebuffer = taillebuffer;
		this.buffer = new Message[this.taillebuffer];
		this.tete = 0;
		this.queue = 0;
		this.mutex = new Semaphore(1);
		this.notFull = new Semaphore(taillebuffer);
		this.notEmpty = new Semaphore(0);
	}

	/**
	 * put m in the prodcons buffer
	 * 
	 * @return
	 **/
	public void put(Message m) throws InterruptedException {
		try {
			notFull.acquire();
			mutex.acquire();
			buffer[queue] = m; // on ajoute m
			queue = ((queue + 1) % taillebuffer);
			mutex.release();
			notEmpty.release();
		} catch (InterruptedException e) {
		}
	}

	/**
	 * retrieve a message from the prodcons buffer, following a fifo order
	 **/
	public Message get() throws InterruptedException {
		Message m = null;
		nbMaxMsg--;
		try {
			notEmpty.acquire();
			mutex.acquire();
			m = buffer[tete]; // on retire notre element du tableau
			tete = ((tete + 1) % taillebuffer);
			mutex.release();
			notFull.release();
		} catch (InterruptedException e) {
		}
		return m;
	}

	/**
	 * returns the number of messages currently available in the prodcons buffer
	 **/
	public int nmsg() {
		return ((queue - tete) % taillebuffer);
	}
}