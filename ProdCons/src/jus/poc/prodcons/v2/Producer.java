package jus.poc.prodcons.v2;

import jus.poc.prodcons.Message;

public class Producer extends Thread {
	private ProdConsBuffer b;
	private int nbmessage;
	int sleep;

	public Producer(ProdConsBuffer b, int n, int s) {
		this.b = b;
		nbmessage = n;
		sleep = s;
	}

	public void run() {
		for (int i = 0; i < nbmessage; i++) {
			try {
				sleep(sleep);
				String s = "'Producer number " + getId() + " -> " + i + "'.";
				Message message = new Message(s);
				b.put(message);
				System.out.println("Producer " + getId() + " : " + s);
			} catch (InterruptedException e) {
			}
		}

	}

}
