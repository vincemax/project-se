package jus.poc.prodcons.v3;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Random;

import jus.poc.prodcons.v3.Producer;

public class TestProdCons {
	public static void main(String args[]) throws InvalidPropertiesFormatException, IOException {

		Properties properties = new Properties();
		properties.loadFromXML(TestProdCons.class.getClassLoader().getResourceAsStream("jus/poc/prodcons/options.xml"));

		int nbP = Integer.parseInt(properties.getProperty("nbP"));
		int nbC = Integer.parseInt(properties.getProperty("nbC"));
		int bufSize = Integer.parseInt(properties.getProperty("BufSz"));
		int Mavg = Integer.parseInt(properties.getProperty("Mavg"));
		int sleepP = Integer.parseInt(properties.getProperty("ProdTime"));
		int sleepC = Integer.parseInt(properties.getProperty("ConsTime"));
		int nbMsg, nbExemplaires;

		System.out.println("Producer to Consumer V3\n");
		System.out.println("Number of Producer : " + nbP);
		System.out.println("Number of Consumer : " + nbC);
		System.out.println("Buffer Size : " + bufSize);
		System.out.println("Average number of messages : " + Mavg + "\n");

		List<Thread> ProdCons = new ArrayList<>();

		ProdConsBuffer b = new ProdConsBuffer(bufSize);

		for (int i = 0; i < nbP; i++) {
			nbMsg = (int) Math.round((((new Random()).nextGaussian() + Mavg)));
			nbExemplaires = (int) Math.round((((new Random()).nextGaussian() + 5)));
			Producer p = new Producer(b, nbMsg, sleepP, nbExemplaires);
			System.out.println("Producer " + p.getId() + " will produce " + nbMsg + " messages.");
			ProdCons.add(p);
			b.nbMaxMsg += nbMsg * nbExemplaires;
		}
		
		System.out.print("\n");
		
		assert(b.nbMaxMsg <= nbC);
		
		for (int i = 0; i < nbC; i++) {
			ProdCons.add(new Consumer(b, sleepC));
		}

		Collections.shuffle(ProdCons);

		Iterator<Thread> parcourir = ProdCons.iterator();
		while (parcourir.hasNext()) {
			parcourir.next().start();
		}
	}
}
