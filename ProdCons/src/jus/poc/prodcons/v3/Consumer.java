package jus.poc.prodcons.v3;

import jus.poc.prodcons.Message;

public class Consumer extends Thread {
	private ProdConsBuffer b;
	int sleep;

	public Consumer(ProdConsBuffer b, int s) {
		this.b = b;
		sleep = s;
	}

	public void run() {
		Message m;
		while (b.nbMaxMsg != 0) {
			try {
				m = b.get();
				sleep(sleep);
				System.out.println("Consumer " + getId() + " : " + m.message);
			} catch (InterruptedException e) {
			}
		}
	}
}
