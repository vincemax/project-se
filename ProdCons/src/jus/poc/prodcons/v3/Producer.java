package jus.poc.prodcons.v3;

import jus.poc.prodcons.Message;

public class Producer extends Thread {
	private ProdConsBuffer b;
	private int nbmessage;
	int sleep;
	int nbExemplaire;

	public Producer(ProdConsBuffer b, int n, int s, int e) {
		this.b = b;
		nbmessage = n;
		sleep = s;
		nbExemplaire = e;
	}

	public void run() {
		for (int i = 0; i < nbmessage; i++) {
			try {
				sleep(sleep);
				String s = "'Producer number " + getId() + " -> " + i + "'";
				Message message = new Message(s,nbExemplaire);
				b.put(message);
				System.out.println("Producer " + getId() + " : " + s + " (x" + nbExemplaire + ").");
			} catch (InterruptedException e) {
			}
		}

	}

}
