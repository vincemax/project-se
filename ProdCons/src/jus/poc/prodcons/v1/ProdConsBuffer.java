package jus.poc.prodcons.v1;

import jus.poc.prodcons.IProdConsBuffer;
import jus.poc.prodcons.Message;

public class ProdConsBuffer implements IProdConsBuffer {
	Message tab[];
	int taillebuffer;
	int tete;
	int queue;
	public int nbMaxMsg = 0;

	public ProdConsBuffer(int taillebuffer) {
		this.taillebuffer = taillebuffer + 1;
		this.tab = new Message[this.taillebuffer];
		this.tete = 0;
		this.queue = 0;
	}

	/**
	 * put m in the prodcons buffer
	 * 
	 * @return
	 **/
	public synchronized void put(Message m) throws InterruptedException {
		while (tete == ((queue + 1) % taillebuffer)) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		tab[queue] = m;
		queue = ((queue + 1) % taillebuffer);
		notifyAll();
	}

	/**
	 * retrieve a message from the prodcons buffer, following a fifo order
	 **/
	public synchronized Message get() throws InterruptedException {
		nbMaxMsg--;
		while (tete == queue) {
			try {
				wait();
			} catch (InterruptedException e) {
			}
		}
		Message m = tab[tete];
		tete = ((tete + 1) % taillebuffer);
		notifyAll();
		return m;
	}

	/**
	 * returns the number of messages currently available in the prodcons buffer
	 **/
	public int nmsg() {
		return ((queue - tete) % taillebuffer);
	}
}
