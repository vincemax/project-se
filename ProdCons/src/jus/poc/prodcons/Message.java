package jus.poc.prodcons;

public class Message {
	public String message;
	public int nbExemplaire;
	
	public Message(String s) {
		message = s;
	}
	
	public Message(String s, int n) {
		message = s;
		nbExemplaire = n;
	}
}
